package service

import (
	"net/http"

	"github.com/gorilla/mux"
)

//Router create router
func Router() http.Handler {
	router := mux.NewRouter()

	router.HandleFunc("/", Check).Methods("GET")
	router.HandleFunc("/in", DataIn).Methods("POST")
	router.HandleFunc("/out", DataOut).Methods("GET")

	return router
}
