package service

import (
	"net/http"
	"fifo_redis/redis"
	"log"
	"io/ioutil"
	"encoding/json"
)

//Check connection health check
func Check(w http.ResponseWriter, _ *http.Request) {
	redisCli := redis.GetClient()
	err := redisCli.Ping().Err()

	w.Header().Set("content-type", "application/json")

	if err != nil {
		log.Println("Failed connection to cache")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("{\"Alive\": false}"))
	}

	w.Write([]byte("{\"Alive\": true}"))
}

//DataIn insert data from request at cache
func DataIn(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	body, err := ioutil.ReadAll(r.Body)
	if err != nil{
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	err = redis.SetData(body)
	if err != nil{
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Write([]byte("{\"Status\": true}"))
}

//DataOut return data from cache list
func DataOut(w http.ResponseWriter, _ *http.Request) {
	data, err := redis.GetData()
	if err != nil{
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	var data1 interface{}

	if err = json.Unmarshal(data, &data1); err != nil{
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(data1)

}
