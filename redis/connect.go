package redis

import (
	"github.com/go-redis/redis"
	"fifo_redis/config"
)

var c *redis.Client

//NewClient open connection to redis client
func NewClient() error {
	client := redis.NewClient(
		&redis.Options{
			Addr:     config.ReadCacheConfig(),
			Password: config.ReadCachePass(),
			DB:       config.ReadCacheDB(),
		})

	err := client.Ping().Err()
	if err != nil {
		return err
	}

	c = client
	return err
}

//Close closes the client
func Close() {
	c.Close()
}

//GetClient return redis client
func GetClient() *redis.Client {
	return c
}
