package redis

import "log"

const queue  = "queue"

//SetData pushed data on redis list
func SetData(data interface{}) error {
	log.Println("Data on set func", data)
	err := c.LPush(queue, data).Err()
	return err
}

//GetData get and remove data from redis list
func GetData() ([]byte, error) {
	data, err := c.RPop(queue).Result()
	res := []byte(data)
	return res, err
}
