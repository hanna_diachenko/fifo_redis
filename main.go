package main

import (
	"log"
	"net/http"
	"os"

	"fifo_redis/config"
	"fifo_redis/service"
	"fifo_redis/redis"
)

var f *os.File // logFile handle

func init() {
	logFile := config.ReadLogFileConfig()
	file, err := os.OpenFile(logFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatal(err)
	}
	f = file

	log.SetOutput(f)

	chErr := redis.NewClient()
	if chErr != nil {
		log.Fatalf("Failed to init cache %v", chErr)
	}
}

func main() {
	defer end()
	defer redis.Close()

	router := service.Router()
	log.Println("API in port 8080")
	log.Println(http.ListenAndServe(":8080", router))
}

func end() {
	if f != nil {
		defer f.Close()
	}
}
