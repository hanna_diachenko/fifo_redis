package config

import (
	"log"
	"os"
	"strconv"
)

const envLogFile = "LOG_FILE"
const envRedisConnection  =  "CACHE_CONNECTION"
const envRedisPass  = "CACHE_PASSWORD"
const envRedisDB = "CACHE_DB"

var defaultValues = map[string]string{
	envLogFile:         "/home/hdiachenko/log/fifo.log",
	envRedisConnection: "localhost:6379",
	envRedisPass: "",
	envRedisDB: "0",
}

//ReadLogFileConfig returns path to log file
func ReadLogFileConfig() string {
	return getString(envLogFile)
}

//ReadCacheConfig returns cache connection string
func ReadCacheConfig() string {
	return getString(envRedisConnection)
}

//ReadCachePass returns cache password string
func ReadCachePass() string {
	return getString(envRedisPass)
}

//ReadCacheDB returns cache db
func ReadCacheDB() int {
	return getInt(envRedisDB)
}

func getString(name string) string {
	var v string
	if v = os.Getenv(name); v != "" {
		return v
	}
	return defaultValues[name]
}

func getInt(name string) int {
	s := getString(name)
	v, err := strconv.Atoi(s)
	if err != nil{
		log.Fatalf("Can's parse %v; error %v", s, err)
	}
	return v
}
